<?php
/**
 * Plugin Name: AL-VR Booking
 * Description: Бронирование времени в игровом зале.
 * Plugin URI:  Ссылка на инфо о плагине
 * Author URI:  https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Author:      Alex Lavigin, Anna Lavigina
 * Version:     1.0
 *
 *
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 *
 */

 /* Тип поста */
define('VR_PTYPE', 'al-vrbooking');




add_action('init', 'vr_init');

function vr_init() {
	
	/* Регистрация типа поста */
	$labels = array(
	
		'name'               => _x('Бронирования', VR_PTYPE, 'al-vrbooking'),
		'singular_name'      => _x('Бронирование', VR_PTYPE, 'al-vrbooking'),
		'menu_name'          => _x('VR Бронирования', 'al-vrbooking', 'al-vrbooking'),
		'name_admin_bar'     => _x('Бронирование', 'al-vrbooking', 'al-vrbooking'),
		'add_new'            => _x('Добавить', 'al-vrbooking', 'al-vrbooking'),
		'add_new_item'       => __('Добавить бронирование', 'al-vrbooking'),
		'new_item'           => __('Добавить', 'al-vrbooking'),
		'edit_item'          => __('Изменить бронирование', 'al-vrbooking'),
		'view_item'          => __('Подробнее', 'al-vrbooking'),
		'all_items'          => __('Все бронирования', 'al-vrbooking'),
		'search_items'       => __('Поиск бронирований', 'al-vrbooking'),
		'not_found'          => __('Бронирований не найдено.', 'al-vrbooking'),
		'not_found_in_trash' => __('Бронирований не найдено.', 'al-vrbooking')
		
	);

	$args = array(
	
		'labels'             => $labels,
        'description'        => __('Описание.', 'vrbooking'),
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array('title')
		
	);
	
	register_post_type(VR_PTYPE, $args);
	
	/* Страница календаря */
	add_action('admin_menu', function(){
	 add_menu_page('NEW VR Booking Календарь', 'NEW VR Календарь', 8, 'new_vr_calendar', 'new_vr_calendar_page', '', 7);
	});
    
    
    /* Колонки таблицы */
    add_filter('manage_'.VR_PTYPE.'_posts_columns', 'vr_columns');

    function vr_columns($columns) {
        
        $columns = array(
        
            'cb' => '<input type="checkbox" />',
            'title' => 'Имя клиента',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'persons' => 'Кол-во посетителей',
            'date_b' => 'Дата бронирования',
            'date_b1' => 'Забронировано на',
            'promo' => 'Промокод',
            'price' => 'Цена',
        
        );
        
        return $columns; 
        
    }

    add_action('manage_'.VR_PTYPE.'_posts_custom_column' , 'vr_column_value', 10, 2);

    function vr_column_value($column, $post_id) {
        
        $name = get_post_meta($post_id, 'vr_name', true);
        $email = get_post_meta($post_id, 'vr_email', true);
        $phone = get_post_meta($post_id, 'vr_phone', true);
        $persons = get_post_meta($post_id, 'vr_persons', true);
        $promo = get_post_meta($post_id, 'vr_promo', true);
        $price = get_post_meta($post_id, 'vr_price', true);
        $dates = get_post_meta($post_id, 'vr_dates', true);
        $data = get_post_meta( $post_id, 'vr_data', true );

        
        if ($column == 'email') {
            echo $email;
        }
        elseif ($column == 'phone') {
            echo $phone;
        }
        elseif ($column == 'persons') {
            echo $persons;
        }
        elseif ($column == 'date_b') {
            
            $tpost = get_post($post_id);
            echo $tpost->post_date;
            
        }
        elseif ($column == 'date_b1') {

            echo "<strong>". $data . "</strong><br>";
            foreach($dates as $date){
                echo $date.'<br>';
            }
            
        }
        elseif ($column == 'promo') {
            echo $promo;
        } 
        elseif ($column == 'price') {
            echo $price;
        }
        
    }

    /* Подключение скриптов и стилей */
    add_action('wp_enqueue_scripts', 'vr_scripts_front');
    add_action('admin_enqueue_scripts', 'vr_scripts');

    function vr_scripts_front() {
        
        wp_enqueue_script('new-vrbooking', plugin_dir_url(__FILE__).'js/build.js', array(), '1.0', true); 
        
    }

    function vr_scripts() {
        wp_enqueue_script('new-vrbooking', plugin_dir_url(__FILE__).'js/build.js', array(), '1.0', true);
        wp_enqueue_style( "bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css");
		wp_enqueue_style( "al_style", plugin_dir_url(__FILE__). "css/style.css");
    }
    
    
    // Order AJAX Start
    add_action('wp_ajax_vr_order_new', 'vr_order_new');
    add_action('wp_ajax_nopriv_vr_order_new', 'vr_order_new');
    
    function vr_order_new(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);
        
        var_dump(json_decode(json_encode($data), true));
        $json_data = json_decode(json_encode($data), true);
        
        $name = $json_data["name"];
        $email = $json_data["email"];
        $phone = $json_data["tel"];
        $persons = $json_data["curentUser"];
        $promo = $json_data["promo"];
        $price =  $json_data["price"];
        $data = $json_data["data"];
        $times = $json_data["time"];
        $timeVisit = array();
        $allTimes  = $json_data["allTime"];
        $callme = $json_data['callme'];

        foreach($times as $time){
            $timeVisit[] = $time["Time"];
        }
        
        /* Добавляем пост */
		$args = array(
		
			'post_title' => $name,
			'post_status' => 'publish',
			'post_type' => VR_PTYPE
		
		);
		
		$post_id = wp_insert_post($args);

        add_post_meta($post_id, 'vr_name', $name);
		add_post_meta($post_id, 'vr_email', $email);
		add_post_meta($post_id, 'vr_phone', $phone);
		add_post_meta($post_id, 'vr_persons', $persons);
		add_post_meta($post_id, 'vr_promo', $promo);
		add_post_meta($post_id, 'vr_price', $price);
        add_post_meta($post_id, 'vr_dates', $timeVisit);
        add_post_meta($post_id, 'vr_data',  $data);
        add_post_meta($post_id, 'vr_allTimes',  $allTimes);
        add_post_meta( $post_id, "vr_callme", $callme);

        $timeVisitToEmail = implode(",", $timeVisit);

        $message = "Имя: " .$name . "<br>";
        $message .= "Email: ". $email . "<br>";
        $message .= "Телефон: ". $phone . "<br>";
        $message .= "Количество Людей: ". $persons . "<br>";
        $message .= "Дата: ".  $data . "<br>";
        $message .= "Время: ". $timeVisitToEmail . "<br>";
        $message .= "Цена: ". $price . "<br>";
        $message .= "Промо код: ". $promo . "<br>";
        $message .= $callme;

        mailto($message);
        sendSMS($name, $persons, $data, $price, $phone, $promo, $timeVisitToEmail, $callme);

        die(json_encode(array('status' => 'ok')));
    }
    // Order AJAX End

    // Send Mail Order start
    function mailto($message){
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        mail("blackfoxvr@gmail.com","Новый заказ", $message, $headers);
    }
    // Send Mail Order End

    // Send SMS Order Start

    function sendSMS($name, $persons, $data, $price, $phone, $promo, $time, $callme){
        include_once(plugin_dir_url(__FILE__).'api/smsc_api.php');

        $mass = $name." %0A";
        $mass .= ($persons == 1 ? $persons.' человек %0A' : $persons.' человека %0A');
        // $mass .= 'Тел.: '. $phone . ' %0A';
        $mass .= 'на '.$data.' %0A';
        $mass .= 'Время:'.$time . '%0A';
        $mass .= $price.' гривен %0A';
        // $mass .= $callme. '%0A';
        $mass .= "Телефон для связи: 0667880009 %0A" ; 
        
		$sms_send = file_get_contents('https://smsc.ru/sys/send.php?login=PavelCher&psw=Pavelcher96&charset=utf-8&phones=38'.$phone.'&mes='.$mass);
        var_dump($mass);
    }

    // Send  SMS Order End



    // Time Taken AJAX Start

    add_action('wp_ajax_vr_time_taken', 'vr_time_taken');
    add_action('wp_ajax_nopriv_vr_time_taken', 'vr_time_taken');

    function vr_time_taken(){
		$query = new WP_Query('post_type=al-vrbooking');

		$sendDataTime = (object) array();

		$allID = array(); // newArray to all ID Post
		
		foreach($query->posts as $id){
			$allID[] = $id->ID;
		}

		$allDataTime = array();
		foreach($allID as $key =>$id){ // Сбор всех зарегистрированных дат
			$data = get_post_meta($id, 'vr_data', true);
			$allTime = get_post_meta( $id, 'vr_allTimes', true );
			$takeTime = get_post_meta( $id, 'vr_dates',	true );

			foreach ($allTime as $key => $value) {
				if ($value['TimeTaken'] == "1"){
					$allTime[$key]['TimeTaken'] = "";
				}
            }
            $allDataTime[] = ["data"=>$data, "dataTime"=>$allTime];
      	
        }
        

		$sendDataTime->dates = $allDataTime;
        die(json_encode(array('status' => 'ok', "body" =>$sendDataTime)));
    }

    // Time Taken AJAX End


    // Promo Code Ajax Start
    add_action('wp_ajax_vr_promo_code', 'vr_promo_code');
    add_action('wp_ajax_nopriv_vr_promo_code', 'vr_promo_code');
    function vr_promo_code(){
        $proms = get_field("promos",'options');
    
        $sendPromos = (object) array();
    
        $promoCodeArr = [];
    
        foreach($proms as  $key => $prom){
            $promoCodeArr[$key] = ['promoCode' => $prom['prmocode'], 'discount' =>$prom['discont']];
        }
    
        $sendPromos->promo = $promoCodeArr;
        
        die(json_encode(array('status' => 'ok', "body" => $sendPromos)));
        }

    // Promo Code Ajax End
    function new_vr_calendar_page() {

            ?>
<div id="app"></div>
<?php
    }

    function al_bookin(){
        return '<div id="app"></div>';
   } 
   add_shortcode('al_bookin', 'al_bookin');
}